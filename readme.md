# JPDB Wikipedia userscript

## About
A userscript that adds Wikipedia entries to the Review, Deck, and Vocab pages of JPDB. 

It's certainly not beautiful code, I hacked this together with ChatGPT; I'm no JavaScript guru, nor do I want to be one.

I checked the limit for the Wikipedia API, and it's 200 requests a second. The script makes at most 50 async requests (an entire deck page) at once, so this is well within the limits. 
See -> [Wikimedia REST API Terms and Conditions](https://www.mediawiki.org/wiki/Wikimedia_REST_API#Terms_and_conditions)

I've tried to make it as fast as possible. It should essentially be instant for the review page, even if it's not cached, as it does the request before the "show answer" button is pressed.
Entries are cached once fetched, and are async on the Deck page.

## Security
Every Wikipedia query result and subsequent storage in the cache is sanitized using Dompurify.sanitize.
This is to mitigate the risk that someone edits a Japanese Wikipedia entry and injects JavaScript into it.
Ontop of that, the script only injects content into the page with .textContent and does not use .innerHtml.

## Compatibility 
- It works fully on Android and Desktop.
- On iOS, the Review page does not work for some reason, and I'm not going to debug it, but if someone wants to fix it and do a pull request, that would be awesome.

## Installation
### !!! IMPORTANT READ THIS FIRST BEFORE INSTALLATION !!!
The userscript manager *cough *Violentmonkey* and perhaps others have a security flaw when it comes to scripts
using *GM.xmlHttpRequest* to allow outgoing requests to other domains (websites). I'll explain the problem and 
solution in detail below.

*GM.xmlHttpRequest* is used by this script to peform connections to Wikipedia to fetch definitions.

#### Problem
JPDB sensibly has a strict CORS policy. This means that when javascript is run on a JPDB page it cannot make outgoing requests to other domains (websites),
this is nice security wise, as if there was ever a XSS vulnerability in the site and malicous code was injected, data could not be exfiltrated as the browser would
block connections from the injected javascript to outgoing domains.

Script managers allow this to be bypassed by implementing the *GM.xmlHttpRequest* function. However some do this more safely (Tampermonkey)
and some do this very badly (Violentmonkey).

What happens when a script installed with Violentmonkey uses *GM.xmlHttpRequest* to do an outgoing request to a different domain (website)?

Well it dosen't notify or ask the user permission to allow outgoing requests to said domain (website).
The issues with this are the following:
- Even if the website has a strong CORS policy, if a user installs a script for the site and does not read the entire script they won't know if the script is making outgoing requests to dodgey domains (websites).
Requests to dodgey websites are commonly used to exfiltrate information captured by malicious javascript.
- Even if the user audits the script on first install, if automatic updates are turnt on, the script could be modified to be malicious, and would start making requests to dodgey domains without
the user knowing.

#### Solution
Use a script manager like Tampermonkey for example, which explicity tells you when new outgoing
connections are being made via *GM.xmlHttpRequest*. After a script is installed and the user navigates to a page where it is run,
when a *GM.xmlHttpRequest* is made a popup box will appear on screen which:
- Shows you the full domain (website) of the outgoing connection
- Asks you to either Deny, Allow once or Always allow outgoing connections to the domain.

In this scenerio, if a script is malciously updated, then when the new connection is being made to
the dodgey domain, a popup will appear asking if you want to connect to said domain. At this point
you can Deny and uninstall the script. 

#### Installation
Open this link and install with script manager: [Link](https://gitlab.com/nakura/jpdb-wiki/-/raw/master/jpdb-wikipedia.user.js?ref_type=heads)
 
Once the script is installed, providing you have a manager which lets you know about new outgoing connections, when you first navigate to one of the pages (Review, Vocab, Deck), your script manager will pop up and ask you to accept outgoing connections to Wikipedia. 
*Double check the domain is Wikipedia* and then select - "always allow from this domain".

With Violentmokey this popup never appears :(.....

## Optional Config
There are 3 user-configurable variables I put at the top of the script:
- The number of entries which are cached - Default is 1000
- The number of lines for the Wikipedia result to show on the Deck page - Default is 4
- Debug logging - Default if false (off)

## Screenshots
## Screenshots
<div style="display: inline-block;">
    <img src="https://gitlab.com/nakura/jpdb-wiki/-/raw/master/images/vocab.png?ref_type=heads&inline=true" width="30%">
</div>
<div style="display: inline-block;">
    <img src="https://gitlab.com/nakura/jpdb-wiki/-/raw/master/images/deck.png?ref_type=heads&inline=true" width="30%">
</div>
<div style="display: inline-block;">
    <img src="https://gitlab.com/nakura/jpdb-wiki/-/raw/master/images/review.png?ref_type=heads&inline=true" width="30%">
</div>
