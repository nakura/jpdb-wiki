// ==UserScript==
// @name         JPDB Wikipedia
// @namespace    jpdb.io
// @version      4.6
// @description  Add a "Wikipedia" section to JPDB vocabulary and review pages with relevant Wikipedia content for the displayed kanji
// @author       胡蝶の夢
// @match        https://jpdb.io/vocabulary/*
// @match        https://jpdb.io/review*
// @match        https://jpdb.io/deck*
// @icon         https://www.google.com/s2/favicons?sz=64&domain=jpdb.io
// @grant        GM.xmlHttpRequest
// @require      https://cdnjs.cloudflare.com/ajax/libs/dompurify/3.1.0/purify.min.js
// @license      MIT
// @run-at       document-end
// ==/UserScript==

(function() {
    'use strict';

    // User changeable
    // Controls the number of lines to display for entries on the deck page
    const maxLinesForDeckEntries = 4;

    // User changeable
    // Controls the maximum number of cache entries
    const maxCacheEntries = 1000;

    // User changeable
    // Controls whether debug logging is enabled or not
    const debugLogEnabled = false;

    function debugLog(string) {
        if (debugLogEnabled) {
            console.log(string);
        }
    }

    // Stores a wikipedia result in the cache
    function setCache(kanji, data) {
        // Check if the cache size limit is reached
        if (Object.keys(localStorage).filter(key => key.startsWith('wikiCache-')).length >= maxCacheEntries) {
            // Remove a random cache entry
            // This is very crude, there must be a much better method....
            const allCacheKeys = Object.keys(localStorage).filter(key => key.startsWith('wikiCache-'));
            const randomKeyToRemove = allCacheKeys[Math.floor(Math.random() * allCacheKeys.length)];
            debugLog(`Cache limit reached. Removing random entry: ${randomKeyToRemove}`);
            localStorage.removeItem(randomKeyToRemove);
        }

        // Set the new cache entry
        debugLog(`Setting cache for ${kanji}`);
        localStorage.setItem(`wikiCache-${kanji}`, JSON.stringify(data));
    }

    // Returns a cached result if one exists or null
    function getCache(kanji) {
        const cachedData = localStorage.getItem(`wikiCache-${kanji}`);
        debugLog(`Getting cache for ${kanji}: `, cachedData);
        return cachedData ? JSON.parse(cachedData) : null;
    }

    // Adds wikipedia results to all deck page entries
    async function addWikipediaExtractToDeckEntries() {
        const vocabularyEntries = document.querySelectorAll('.entry');
        debugLog(`Found ${vocabularyEntries.length} vocabulary entries.`);

        const kanjiTexts = Array.from(vocabularyEntries).map(entry => {
            const kanjiElement = entry.querySelector('.vocabulary-spelling a');
            if (!kanjiElement) return null;

            const furiganaRemovedText = kanjiElement.cloneNode(true);
            furiganaRemovedText.querySelectorAll('rt').forEach(rt => rt.remove());
            return {
                kanjiText: furiganaRemovedText.textContent.trim(),
                entry
            };
        }).filter(kanji => kanji !== null);

        const wikiResponses = await Promise.all(kanjiTexts.map(kanji =>
            queryWikipedia(kanji.kanjiText).then(response => ({
                kanji: kanji.kanjiText,
                response,
                entry: kanji.entry
            }))
        ));

        wikiResponses.forEach(({
            kanji,
            response,
            entry
        }) => {
            debugLog(`Processing kanji: ${kanji}`);
            if (!response) return;

            const wikiPage = response.query?.pages && Object.values(response.query.pages)[0];
            const extractText = wikiPage?.extract;
            if (extractText) {
                debugLog(`Extract found for ${kanji}: ${extractText}`);
                const sentences = extractText.split('。').slice(0, maxLinesForDeckEntries).join('。') + (extractText.split('。').length > maxLinesForDeckEntries ? '。' : '');

                const wikiExtractDiv = document.createElement('div');
                wikiExtractDiv.textContent = `wikipedia: ${sentences}`;
                wikiExtractDiv.className = 'wikipedia-extract';

                // Apply margin-top directly using the style property
                // Adds a space between native content and wikipedia content
                wikiExtractDiv.style.marginTop = '1em'; // Adjust the value as needed

                const textDescriptionDiv = entry.querySelector('div:not(.vocabulary-spelling):not(.dropdown):not(.tags)');
                if (textDescriptionDiv) {
                    textDescriptionDiv.appendChild(wikiExtractDiv);
                } else {
                    debugLog('Text description div not found for', kanji);
                }
            } else {
                debugLog(`No extract found for ${kanji}`);
            }
        });
    }

    // If entry is in cache, returns entry otherwise queries wikipedia, stores entry in cache the returns entry 
    async function queryWikipedia(kanji) {
        // Check cache first
        const cachedData = getCache(kanji);
        if (cachedData) {
            debugLog('Using cached response for:', kanji);
            return cachedData;
        }

        try {
            const url = `https://ja.wikipedia.org/w/api.php?action=query&format=json&prop=extracts&titles=${encodeURIComponent(kanji)}&exintro=1&explaintext=1`;
            const response = await new Promise((resolve, reject) => {
                GM.xmlHttpRequest({
                    method: "GET",
                    url: url,
                    onload: resolve,
                    onerror: reject
                });
            });
            // sanitize response and store in cache
            const jsonResponse = JSON.parse(DOMPurify.sanitize(response.responseText));
            setCache(kanji, jsonResponse);
            return jsonResponse;
        } catch (error) {
            console.error('Error fetching Wikipedia content:', error);
            return null;
        }
    }

    function createSection(headingText, contentText) {
        const section = document.createElement('div');
        section.className = 'subsection-alt-meanings';

        const heading = document.createElement('h6');
        heading.className = 'subsection-label';
        heading.textContent = headingText;
        section.appendChild(heading);

        const content = document.createElement('div');
        content.className = 'subsection';
        content.textContent = contentText;
        section.appendChild(content);

        return section;
    }

    async function addAltMeaningsSection(element, kanji) {
        if (!kanji) return;

        let wikiResponse = await queryWikipedia(kanji);

        const wikiPage = wikiResponse?.query?.pages && Object.values(wikiResponse.query.pages)[0];
        const extractText = wikiPage?.extract;
        if (extractText) {
            const wikiSection = createSection('Wikipedia', extractText);
            element.parentNode.insertBefore(wikiSection, element.nextSibling);
        }
    }

    function extractKanjiFromElement(element, selector) {
        const kanjiElement = element.querySelector(selector);
        if (!kanjiElement) return '';

        const clonedElement = kanjiElement.cloneNode(true);
        clonedElement.querySelectorAll('rt').forEach(rt => rt.remove());

        return clonedElement.textContent.trim();
    }

    function getCurrentPageKanji() {
        if (window.location.href.includes("/vocabulary/")) {
            return document.querySelector('#q')?.value.trim() || '';
        } else if (window.location.href.includes("/review")) {
            return document.querySelector('.review-hidden .plain > div:nth-child(3)')?.textContent.trim() || '';
        }
        return '';
    }


    function observeWordAppearance(kanji, showAnswerButton) {
        const observer = new MutationObserver(mutations => {
            for (const mutation of mutations) {
                if (mutation.addedNodes.length > 0 && document.querySelector('.review-reveal .answer-box a[href^="/vocabulary"]')) {
                    observer.disconnect();
                    const meaningsSection = document.querySelector('.subsection-meanings');
                    if (kanji && meaningsSection) {
                        addAltMeaningsSection(meaningsSection, kanji);
                    }
                    break;
                }
            }
        });

        observer.observe(document.body, {
            childList: true,
            subtree: true
        });
    }

    function getCurrentPageWord() {
        debugLog("Getting current page word.");
        if (window.location.href.includes("/vocabulary/")) {
            return document.querySelector('#q')?.value.trim() || '';
        } else if (window.location.href.includes("/review")) {
            return document.querySelector('.review-hidden .plain > div:nth-child(3)')?.textContent.trim() || '';
        }
        return '';
    }

    // Function to handle virtual-refresh event
    function handleVirtualRefresh() {
        document.addEventListener('virtual-refresh', function() {
            debugLog('Page content updated, reapplying Wikipedia definitions');
            addWikipediaExtractToDeckEntries();
        });
    }


    async function initializeScript() {
        if (window.location.href.includes("/vocabulary/")) {
            const handleVocabularyPage = () => {
                const kanji = getCurrentPageKanji();
                debugLog('Got vocab page kanji: ', kanji);
                const meaningsSection = document.querySelector('.subsection-meanings');
                if (kanji && meaningsSection) {
                    addAltMeaningsSection(meaningsSection, kanji);
                }
            };

            document.readyState === 'loading' ?
                document.addEventListener('DOMContentLoaded', handleVocabularyPage) :
                handleVocabularyPage();
        } else if (window.location.href.includes("/review")) {
            // Logic for review pages
            const word = getCurrentPageWord();
            queryWikipedia(word);

            const handleReviewPage = () => {
                const showAnswerButton = document.querySelector('#show-answer');
                if (showAnswerButton) {
                    showAnswerButton.addEventListener('click', () => {
                        observeWordAppearance(word, showAnswerButton);
                    });
                } else {
                    debugLog('Show answer button not found');
                }
            };

            document.readyState === 'loading' ?
                document.addEventListener('DOMContentLoaded', handleReviewPage) :
                handleReviewPage();
        } else if (window.location.href.includes("/deck")) {
            // Logic for deck pages
            const handleDeckPage = () => {
                addWikipediaExtractToDeckEntries();
            };

            document.readyState === 'loading' ?
                document.addEventListener('DOMContentLoaded', handleDeckPage) :
                handleDeckPage();
        }

        // Listen for virtual-refresh event across all pages
        document.addEventListener('virtual-refresh', function() {
            debugLog('Page content updated, reapplying Wikipedia definitions');
            // Determine the current page and reapply the appropriate function
            if (window.location.href.includes("/vocabulary/")) {
                const kanji = getCurrentPageKanji();
                const meaningsSection = document.querySelector('.subsection-meanings');
                if (kanji && meaningsSection) {
                    addAltMeaningsSection(meaningsSection, kanji);
                }
            } else if (window.location.href.includes("/review")) {
                const word = getCurrentPageWord();
                const meaningsSection = document.querySelector('.subsection-meanings');
                if (word && meaningsSection) {
                    addAltMeaningsSection(meaningsSection, word);
                }
            } else if (window.location.href.includes("/deck")) {
                addWikipediaExtractToDeckEntries();
            }
        });
    }

    // Run the script
    initializeScript();
})();
